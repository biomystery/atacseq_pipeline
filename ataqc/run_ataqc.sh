#!/bin/bash -l

# This script is an example test of the ataqc package

# Don't forget to run `export -f module` first
source activate bds_atac


# Directories and prefixes
WORKDIR="$PWD"
OUTDIR="./qc2/"
OUTPREFIX="atac_jyh_16"
INPREFIX="JYH_16"
GENOME='hg38' 

# Annotation files
ANNOTDIR="/projects/ps-epigen/GENOME/"
DNASE_BED="${ANNOTDIR}/${GENOME}/ataqc/reg2map_honeybadger2_dnase_all_p10_ucsc.hg19_to_hg38.bed.gz"
BLACKLIST_BED="${ANNOTDIR}/${GENOME}/hg38.blacklist.bed.gz"
TSS_BED="${ANNOTDIR}/${GENOME}/ataqc/hg38_gencode_tss_unique.bed.gz"
REF_FASTA="${ANNOTDIR}/${GENOME}/GRCh38_no_alt_analysis_set_GCA_000001405.15.fasta"
PROM="${ANNOTDIR}/${GENOME}/ataqc/reg2map_honeybadger2_dnase_prom_p2.hg19_to_hg38.bed.gz"
ENH="${ANNOTDIR}/${GENOME}/ataqc/reg2map_honeybadger2_dnase_enh_p2.hg19_to_hg38.bed.gz"
chrsz="${ANNOTDIR}/${GENOME}/hg38.chrom.sizes"
pbc_log="${WORKDIR}/qc/rep1/JYH_16sub_R1.PE2SE.nodup.pbc.qc"
bam="${WORKDIR}/align/rep1/JYH_16sub_R1.PE2SE.bam"
align_log="${WORKDIR}/qc/rep1/JYH_16sub_R1.PE2SE.align.log"
dup_log="${WORKDIR}/qc/rep1/JYH_16sub_R1.PE2SE.dup.qc"
filt_bam="${WORKDIR}/align/rep1/JYH_16sub_R1.PE2SE.nodup.bam"
bed="${WORKDIR}/align/rep1/JYH_16sub_R1.PE2SE.nodup.bedpe.gz"
#bigwig="${WORKDIR}/signal/macs2/rep1/JYH_16sub_R1.PE2SE.nodup.tn5.pf.pval.signal.bigwig"

python ~/software/atacseq_pipeline/ataqc/run_ataqc.py \
                    --workdir $WORKDIR \
                    --outdir $OUTDIR \
                    --outprefix $OUTPREFIX \
                    --genome $GENOME \
		    --chromsizes $chrsz \
                    --ref $REF_FASTA \
                    --dnase $DNASE_BED \
                    --tss $TSS_BED \
                    --blacklist $BLACKLIST_BED \
                    --prom $PROM \
                    --enh $ENH \
		    --pbc $pbc_log\
                    --fastq1 /home/zhc268/scratch/seqdata/1M_sample/JYH_16sub_R1.fastq\
                    --fastq2 /home/zhc268/scratch/seqdata/1M_sample/JYH_16sub_R2.fastq\
		    --alignedbam $bam \
		    --alignmentlog $align_log \
		    --coordsortbam $bam \
		    --duplog $dup_log \
		    --finalbam $filt_bam \
		    --finalbed $bed \
                    --inprefix $INPREFIX 2>  $OUTDIR"./runErr.txt"


source deactivate bds_atac
