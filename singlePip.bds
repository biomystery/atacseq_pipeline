#!/usr/bin/env bds
#vim: syntax=java


help == atac pipeline settings

type 		:= "atac-seq-single" help Type of the pipeline. atac-seq or dnase-seq (default: atac-seq).
align	 	:= false	help Align only (no MACS2 peak calling or IDR or ataqc analysis).
subsample_xcor	:= "25M"	help # reads to subsample for cross corr. analysis (default: 25M).
subsample 	:= "0" 		help # reads to subsample exp. replicates. Subsampled tagalign will be used for steps downstream (default: 0; no subsampling).
no_ataqc 	:= false 	help No ATAQC
no_xcor 	:= false 	help No Cross-correlation analysis.
smooth_win 	:= "150" 	help Smoothing window size for MACS2 peak calling (default: 150).
no_browser_tracks := false 	help Disable generation of genome browser tracks (workaround for bzip2 shared library issue).
macs2_pval_thresh := 0.01	help MACS2 p-val threshold for calling peaks (default: 0.1).
macs2_pval_thresh_bw := 0.01	help MACS2 p-val threshold for generating BIGWIG signal tracks (default: 0.1).


help() // show help contexts

include "modules/pipeline_template.bds"
include "modules/input.bds"
include "modules/input_adapter.bds"

include "modules/align_bowtie2.bds"
include "modules/align_etc.bds"

include "modules/postalign_bam.bds"
include "modules/postalign_bed.bds"
include "modules/postalign_xcor.bds"

include "modules/callpeak_macs2_atac.bds"
include "modules/callpeak_filter.bds"
include "modules/callpeak_blacklist_filter.bds"
include "modules/callpeak_bigbed.bds"

include "modules/ataqc.bds"

// Important output file names are stored in global variables (usually a string map string{} with a key with replicate id, pair id)
// e.g. filt_bam{"1"} = filtered bam for replicate 1, peak_pr1{"2"} = peak file for pseudo replicate 1 of replicate 2

string{} fastq, align_log, flagstat_qc, bam, filt_bam, dup_qc, flagstat_nodup_qc, pbc_qc, xcor_qc, xcor_plot
string{} final_tag, final_tag_pr1, final_tag_pr2

string{} peak, peak_001

string num_peak_log

string{} pval_bigwig_001, fc_bigwig_001

string{} ataqc_qc

main()


void main() { // atac pipeline starts here

	init_atac()

	chk_param() // check if parameters are valid

	chk_input( true, false ) 

	do_align()

	call_peaks()

	log_number_of_peaks()

	ataqc()

	// blacklist-filter peaks and also make ENCODE accession metadata for them
	filter_peak_and_convert_to_bigbed() 

	tar_all_logs()

	report()
}

void init_atac() {

	read_conf_atac()

	init_etc_atac()

	print_atac()

	init_filetable()
}

void read_conf_atac() {

	type		= get_conf_val( type,			["type"] )
	trimmed_fastq 	= get_conf_val_bool( trimmed_fastq,	["trimmed_fastq"] )
	align		= get_conf_val_bool( align,		["align"] )
	no_ataqc 	= get_conf_val_bool( no_ataqc, 		["no_ataqc"] )
	no_xcor 	= get_conf_val_bool( no_xcor, 		["no_xcor"] )
	smooth_win	= get_conf_val( smooth_win,		["smooth_win"] )
	subsample_xcor 	= get_conf_val( subsample_xcor,		["subsample_xcor"] )
	subsample	= get_conf_val( subsample, 		["subsample"] )
	no_browser_tracks = get_conf_val_bool( no_browser_tracks,	["no_browser_tracks"] )
	macs2_pval_thresh = get_conf_val_real( macs2_pval_thresh, ["macs2_pval_thresh"] )
	macs2_pval_thresh_bw = get_conf_val_real( macs2_pval_thresh_bw, ["macs2_pval_thresh_bw"] )
}

void init_etc_atac() {

	default_is_pe 	= true
	speak_xcor 	= 0 // set fragment length explicitly as zero for cross corr. analysis
	if ( rm_chr_from_tag == "" ) rm_chr_from_tag = "chrM"; // remove lines with chrM in _bam_to_tag ??
}

void print_atac() {

	print( "\n\n== atac pipeline settings\n")
	print( "Type of pipeline\t\t\t: $type\n")
	print( "Fastqs are trimmed?\t\t\t: $trimmed_fastq\n")
	print( "Align only\t\t\t\t: " + align + "\n")
	print( "# reads to subsample replicates (0 if no subsampling)\t: "+parse_number( subsample )+"\n")
	print( "# reads to subsample for cross-corr. analysis \t: " +parse_number( subsample_xcor)+"\n")
	 print( "No ATAQC (advanced QC report)\t\t: $no_ataqc\n")
	 print( "No Cross-corr. analysis\t\t\t: $no_xcor\n")
	 print( "Smoothing window for MACS2\t\t: $smooth_win\n")
	 print( "Disable genome browser tracks\t: $no_browser_tracks\n" )
	 print( "MACS2 p-val thresh. for peaks\t: $macs2_pval_thresh\n")
	 print( "MACS2 p-val thresh. for BIGWIGs\t\t: $macs2_pval_thresh_bw\n")

 }


 void init_filetable() { // init file table labels in HTML report

	 // add label to graphviz
	 // : Items in filetable will be sorted in the ascending order of rank
	 // : Items added later will have higher rank

	 // Level 1
	 add_label_to_table("Raw reads")
	 add_label_to_table("Alignment")
	 add_label_to_table("Signal tracks")
	 add_label_to_table("Peaks")
	 add_label_to_table("QC and logs")

	 // Level 2
	 for (int i=1; i<=100; i++) \
		 Add_Label_to_table("Replicate $i")

	 add_label_to_table("True replicates")
	 add_label_to_table("MACS2")


	 // Higher levels
	 add_label_to_table("Fastq")
	 add_label_to_table("Fastq 1")
	 add_label_to_table("Fastq 2")
	 add_label_to_table("Bowtie2 map. log")
	 add_label_to_table("Bam")
	 add_label_to_table("Filtered bam")
	 add_label_to_table("Sorted bam")
	 add_label_to_table("Dedup. log")
	 add_label_to_table("Bowtie2 map. flagstat log")
	 add_label_to_table("PBC log")
	 add_label_to_table("Bedpe")
	 add_label_to_table("Subsampled bedpe")
	 add_label_to_table("Tag-align")
	 add_label_to_table("Subsampled tag-align")
	 add_label_to_table("Cross-corr. log")
	 add_label_to_table("Cross-corr. plot")
	 add_label_to_table("P-value")
	 add_label_to_table("Fold enrichment")
	 add_label_to_table("Narrow peak")
	 add_label_to_table("Filtered narrow peak")
	 add_label_to_table("Peak")
	 add_label_to_table("Filtered peak")
	 add_label_to_table("ATAQC")

	 // add label to graphviz (short name, long name)

	 for (int i=1; i<=50; i++) {
		 add_label_to_graph("rep$i", "Replicate $i")
		 add_label_to_graph("rep$i-pr1", "Pseudo-replicate 1 for rep. $i")
		 add_label_to_graph("rep$i-pr2", "Pseudo-replicate 2 for rep. $i")
		 add_label_to_graph("rep$i-pr", "Pseudo replicates for rep. $i")
		 for (int j=1; j<=20; j++) {
			 add_label_to_graph("rep$i-rep$j", "Rep. $i vs. Rep. $j")
		 }
	 }
 }

 void chk_param() {

	 print( "\n== checking atac parameters ...\n" );

	 if ( has_input_fastq() ) chk_align_bwt2()
	 if ( !align ) 		chk_callpeak_macs2()
	 if ( !no_ataqc ) {
		 no_ataqc = !chk_ataqc()
	 }

	 if ( has_pe_input_tag() && subsample > 0 ) {
		 print("Warning: Cannot subsample paired end tagaligns. Disabling subsampling...\n")
		 subsample = 0
	 }

	 if ( !has_input_fastq() && !no_ataqc ) {
		 print("Warning: ATAQC is available for fastq inputs only. Disabling ATAQC...\n")
		 no_ataqc = true
		     } //what is has_input_fastq 


	 //ENCODE_assay_category = "DNA accessibility"
	 ENCODE_assay_title = "ATAC-seq"

 }


 void do_align() {

	 if ( is_input_peak() ) return

	 // filesize of input ( map with key $rep )
	 int{} filesize

	 for ( int rep=1; rep <= get_num_rep(); rep++) {

		 // check file size to distribute nth to each nth_app
		 // determine # threads for each app related to alignment

		 // get file size in bytes
		 if ( is_input_fastq( rep ) ) {

			 fastqs := get_fastqs( rep )
			 filesize{rep} = (fastqs[0]).size()
			 if ( fastqs.size() > 1) filesize{rep} += (fastqs[1]).size()*3 // multiply 3 to allocate more cpus for align
		 }
		 else if ( is_input_bam( rep ) ) 	filesize{rep} = (get_bam( 0, rep )).size()
		 else if ( is_input_filt_bam( rep ) ) 	filesize{rep} = (get_filt_bam( 0, rep )).size()
		 else if ( is_input_tag( rep ) ) 	filesize{rep} = (get_tag( 0, rep )).size()*10
	 }

	 //// distribute # threads for each replicate
	 nth_rep := distribute_nonzero( nth, filesize ) // distribute # threads according to input filesize

	 for (int rep=1; rep<=get_num_rep(); rep++) {

		 if ( no_par ) do_align( rep, nth_rep{rep} )
		 else 	  par do_align( rep, nth_rep{rep} )
	 }

	 wait

	 print( "\n== Done do_align()\n" )
 }

 void do_align( int rep, int nth_rep ) {
     align_PE( rep, nth_rep )
 }


void align_PE( int rep, int nth_rep ) {

	group 	:= get_group_name( rep )
	long 	:= get_long_group_name( rep )

	aln_o_dir := mkdir( "$out_dir/align/$group" ) // create align output directory
	qc_o_dir  := mkdir( "$out_dir/qc/$group" ) // create qc output dir.

	string bam_, align_log_, read_length_log, flagstat_qc_
	string[] fastqs_pair1, fastqs_pair2

	if ( is_input_fastq( rep ) ) {

		fastqs_pair1 = get_fastq( 0, rep, 1 )
		fastqs_pair2 = get_fastq( 0, rep, 2 )

		string[] trimmed_fastqs_pair1, trimmed_fastqs_pair2

		if ( fastqs_pair1.size() != fastqs_pair2.size() ) {
			error("Number of fastqs to be pooled for pair 1 and pair 2 do not match!\n")
		}
		for ( int i=0; i<fastqs_pair1.size(); i++) {
			id := i+1
			suffix := fastqs_pair1.size()==1 ? "" : ":$id"


			trimmed_fastqs_pair1.add( fastqs_pair1[i] )
			trimmed_fastqs_pair2.add( fastqs_pair2[i] )

			}
			//if ( i==0 ) read_length_log = get_read_length_log( trimmed_fastqs_pair1[0], qc_o_dir, group )
		}
		wait

		string p1, p2
		if ( trimmed_fastqs_pair1.size() > 1 ) { // if multiple fastqs are given, pool trimmed fastqs
			p1 = pool_fastq( trimmed_fastqs_pair1, aln_o_dir, group )
			p2 = pool_fastq( trimmed_fastqs_pair2, aln_o_dir, group )
			// add_file_to_report( p1, "pooled\\nfastq 1" , group, "Raw reads/$long/Pooled fastq 1" )
			// add_file_to_report( p2, "pooled\\nfastq 2" , group, "Raw reads/$long/Pooled fastq 2" )
			wait
		}
		else {
			p1 = trimmed_fastqs_pair1[0]
			p2 = trimmed_fastqs_pair2[0]
		}

		fastq{rep+",1"} = p1
		fastq{rep+",2"} = p2

		read_length_log = get_read_length_log( p1, qc_o_dir, group )
		( bam_, align_log_ ) = bowtie2_PE( p1, p2, aln_o_dir, qc_o_dir, group, nth_rep )
		wait
		align_log{rep} = align_log_
		add_file_to_table( align_log_, "QC and logs/$long/Bowtie2 map. log")

		flagstat_qc_ = samtools_flagstat_bam( bam_, qc_o_dir, group )
		wait
		flagstat_qc{rep} = flagstat_qc_
		add_file_to_table( flagstat_qc_, "QC and logs/$long/Bowtie2 map. flagstat log")

		// add to report
		tmp_log := parse_flagstat( flagstat_qc_ )
		raw_reads := metric_prefix( parse_int( tmp_log{"total"} ) )
		half_raw_reads := metric_prefix( parse_int( tmp_log{"total"} )/2 )
		
		for ( int i=0; i<fastqs_pair1.size(); i++) {
			if ( trimmed_fastq ) {
				add_file_to_report( fastqs_pair1[i], "fastq 1" + (half_raw_reads ? "\\n$half_raw_reads" : ""), group, \
					"Raw reads/$long/Fastq 1"+ (half_raw_reads ? " ($half_raw_reads)" : "") )
				add_file_to_report( fastqs_pair2[i], "fastq 2" + (half_raw_reads ? "\\n$half_raw_reads" : ""), group, \
					"Raw reads/$long/Fastq 2"+ (half_raw_reads ? " ($half_raw_reads)" : "") )
			}
			else {
				add_file_to_report( fastqs_pair1[i], "fastq 1", group, \
					"Raw reads/$long/Fastq 1" )
				add_file_to_report( fastqs_pair2[i], "fastq 2", group, \
					"Raw reads/$long/Fastq 2" )
				add_file_to_report( trimmed_fastqs_pair1[i], "trimmed\\nfastq 1" + (half_raw_reads ? "\\n$half_raw_reads" : ""), group, \
					"Raw reads/$long/Trimmed fastq 1"+ (half_raw_reads ? " ($half_raw_reads)" : "") )
				add_file_to_report( trimmed_fastqs_pair2[i], "trimmed\\nfastq 2" + (half_raw_reads ? "\\n$half_raw_reads" : ""), group, \
					"Raw reads/$long/Trimmed fastq 2"+ (half_raw_reads ? " ($half_raw_reads)" : "") )
			}
		}
		

		mapped_reads := metric_prefix( parse_int( tmp_log{"mapped"} ) )
		bam{rep} = bam_
		add_file_to_report( bam_, "bam" + (mapped_reads ? "\\n$mapped_reads" : ""), group, \
			"Alignment/$long/Bam" + (mapped_reads ? " ($mapped_reads)" : "") )
	}

	string filt_bam_, dup_qc_, pbc_qc_, flagstat_nodup_qc_

	if ( is_input_fastq( rep ) || is_input_bam( rep ) ) {

		if ( is_input_bam( rep ) ) {
			bam_ = get_bam( 0, rep )
			bam{rep} = bam_
		}

		string deduped_reads
		if ( no_dup_removal ) {
			string tmp
			(filt_bam_, tmp ) \
				= dedup_bam_PE( bam_, aln_o_dir, qc_o_dir, group, nth_rep )
			wait
		}
		else {
			(filt_bam_, dup_qc_, flagstat_nodup_qc_, pbc_qc_ ) \
				= dedup_bam_PE( bam_, aln_o_dir, qc_o_dir, group, nth_rep )
			dup_qc{rep} = dup_qc_
			pbc_qc{rep} = pbc_qc_
			flagstat_nodup_qc{rep} = flagstat_nodup_qc_
			add_file_to_table( dup_qc_, "QC and logs/$long/Dedup. log")
			add_file_to_table( pbc_qc_, "QC and logs/$long/PBC log")
			add_file_to_table( flagstat_nodup_qc_, "QC and logs/$long/Filtered flagstat log")
			wait
			tmp_log := parse_flagstat( flagstat_nodup_qc_ )
			deduped_reads = metric_prefix( parse_int( tmp_log{"total"} ) )			
		}
		// add to report
		filt_bam{rep} = filt_bam_
		add_file_to_report( filt_bam_, "filt. bam" + (deduped_reads ? "\\n$deduped_reads" : ""), group, \
			"Alignment/$long/Filtered & deduped bam" + (deduped_reads ? " ($deduped_reads)" : "") )

		if ( is_input_fastq( rep ) ) {
			string ENCODE_step_name
			if ( get_num_rep()==1 ) ENCODE_step_name = "anshul-kundaje:atac-seq-trim-align-filter-step-run-single-rep-v1"
			else 			ENCODE_step_name = "anshul-kundaje:atac-seq-trim-align-filter-step-run-v1"
			if ( fastqs_pair1.size() > 0 || fastqs_pair2.size() > 0 ) {
				add_ENCODE_metadata_to_summary_json( "bam", "", "alignments", \
					ENCODE_step_name, filt_bam_, fastqs_pair1+fastqs_pair2 )
			}
			if ( flagstat_qc_) { 
				add_ENCODE_quality_metrics_to_summary_json( "samtools_flagstats_quality_metric", \
					ENCODE_step_name, [filt_bam_], [flagstat_qc_] )
			}
		}
	}

	string bedpe, subsampled_bedpe, tag

	if ( is_input_fastq( rep ) || is_input_bam( rep ) || is_input_filt_bam( rep ) ) {

		if ( is_input_filt_bam( rep ) ) {
			filt_bam_ = get_filt_bam( 0, rep )
			filt_bam{rep} = filt_bam_
		}

		bedpe = bam_to_bedpe( filt_bam_, aln_o_dir, group )
		wait

		if ( parse_number( subsample )!=0 ) {

			subsampled_bedpe = subsample_bedpe( bedpe, parse_number( subsample ), aln_o_dir, group )
		}
		else {
			subsampled_bedpe = bedpe
		}
		wait

		tag = bedpe_to_tag( subsampled_bedpe, aln_o_dir, group )
		wait
	}

	string final_tag_, final_tag_pr1_, final_tag_pr2_

	if ( is_input_fastq( rep ) || is_input_bam( rep ) || is_input_filt_bam( rep ) || is_input_tag( rep ) ) {

		if ( is_input_tag( rep ) ) tag = get_tag( 0, rep )

		string aln_pr1_o_dir, aln_pr2_o_dir
		string tag_pr1, tag_pr2

		 //if ( !true_rep ) {

		aln_pr1_o_dir = mkdir( "$out_dir/align/pseudo_reps/$group/pr1" )
		aln_pr2_o_dir = mkdir( "$out_dir/align/pseudo_reps/$group/pr2" )
					       
		if ( is_input_tag( rep ) ) {
				( tag_pr1, tag_pr2 ) = spr_tag_PE( tag, aln_pr1_o_dir, aln_pr2_o_dir, group )
		}
		else {
			( tag_pr1, tag_pr2 ) = spr_PE( subsampled_bedpe, aln_pr1_o_dir, aln_pr2_o_dir, group )
		}
		wait

		//
		final_tag_ = tn5_shift_tag( tag, aln_o_dir, group )

		final_tag{rep} = final_tag_

		add_file_to_report( final_tag_, "tag-align", group, "Alignment/$long/Tag-align" )

		 //if ( !true_rep ) {
				     
   	        final_tag_pr1_ = tn5_shift_tag( tag_pr1, aln_pr1_o_dir, group )
		final_tag_pr2_ = tn5_shift_tag( tag_pr2, aln_pr2_o_dir, group )

		final_tag_pr1{rep} = final_tag_pr1_
		final_tag_pr2{rep} = final_tag_pr2_

		add_file_to_report( final_tag_pr1_, "tag-align", "$group-pr1", \
				"Alignment/Pseudo-replicates/$long/Pseudo-replicate 1/Tag-align" )
		add_file_to_report( final_tag_pr2_, "tag-align", "$group-pr2", \
				"Alignment/Pseudo-replicates/$long/Pseudo-replicate 2/Tag-align" )			
						 //}
		wait

		string subsampled_tag_xcor

		if ( bedpe == "" ) {
			subsampled_tag_xcor = subsample_tag_PE_xcor( tag, parse_number( subsample_xcor ), aln_o_dir, group )
		}
		else {
			subsampled_tag_xcor = subsample_bedpe_to_tag_xcor( bedpe, parse_number( subsample_xcor ), aln_o_dir, group )
		}
		wait

		if ( !no_xcor ) {
			// cross-corr. analysis
			string xcor_qc_, xcor_plot_
			( xcor_qc_, xcor_plot_ ) = xcor( subsampled_tag_xcor, qc_o_dir, group, nth_rep ) 

			xcor_qc{rep} = xcor_qc_
			xcor_plot{rep} = xcor_plot_

			add_file_to_report( final_tag_, "tag-align", group, "Alignment/$long/Tag-align" )
			add_file_to_table( xcor_plot_, "QC and logs/$long/Cross-corr. plot" )

			wait
			string ENCODE_step_name
			if ( pbc_qc_ && read_length_log ) {
				if ( get_num_rep() == 1 ) \
					ENCODE_step_name = "anshul-kundaje:atac-seq-trim-align-filter-step-run-single-rep-v1"
				else 			  \
					ENCODE_step_name = "anshul-kundaje:atac-seq-trim-align-filter-step-run-v1"			
				add_ENCODE_quality_metrics_to_summary_json( "complexity_xcorr_quality_metric", \
					ENCODE_step_name, \
					[filt_bam_], [pbc_qc_, xcor_qc_, read_length_log], [ "true", xcor_plot_] )
			}
		}
	}



void call_peaks() { // for pooling two replicates and calling peaks on them

	if ( align ) return
	if ( is_input_peak() ) return


	string tmp
	// call peaks for each replicate
	for ( int rep=1; rep<=get_num_rep(); rep++ ) {
		group 	:= get_group_name( rep )
		long 	:= get_long_group_name( rep )

		// call peaks
		peak_o_dir 	:= mkdir( "$out_dir/peak/macs2/$group")
		sig_o_dir 	:= mkdir( "$out_dir/signal/macs2/$group" )

		// signal track generation = true
		string peak_001_
		( peak_001_, fc_bigwig_001{rep}, pval_bigwig_001{rep} ) \
				= macs2_atac_npeak_and_signal( final_tag{rep}, "$smooth_win", macs2_pval_thresh_bw, true, peak_o_dir, sig_o_dir, group )
		peak_001{rep} = peak_001_
		add_file_to_report( peak_001{rep}, "n. peak\\np-val<$macs2_pval_thresh_bw", group, \
			"Peaks/MACS2/$long/Narrow peak (p-val thresh=$macs2_pval_thresh_bw)" )
		add_file_to_report( fc_bigwig_001{rep}, "signal fc", group, "Signal tracks/MACS2/$long/Fold enrichment" )
		add_file_to_report( pval_bigwig_001{rep}, "signal p-val", group, "Signal tracks/MACS2/$long/P-value" )

		if ( macs2_pval_thresh_bw == macs2_pval_thresh ) { // if two p-val threshold are the same, skip one of them.			
			peak{rep} = peak_001_
		}
		else {
			( peak{rep}, tmp )  \
					= macs2_atac_npeak_and_signal( final_tag{rep}, "$smooth_win", macs2_pval_thresh, false, peak_o_dir, sig_o_dir, group )
			add_file_to_report( peak{rep}, "n. peak", group, "Peaks/MACS2/$long/Narrow peak" )

		}

	}

	// call peaks for pooled replicates

	wait

	print( "\n== Done call_peaks()\n" )
}


void log_number_of_peaks() {
	if ( align ) return

	log_o_dir := mkdir("$out_dir/qc")
	num_peak_log = "$log_o_dir/" + (title ? (title+"_") : "" ) + "number_of_peaks.txt"
	string lines
	for ( int rep=1; rep<=get_num_rep(); rep++) { // rep==0 : pooled
		if ( peak.hasKey(rep) ) \
			lines += "rep$rep\t"+get_num_lines( peak{rep} )+"\n"
	}

	num_peak_log.write(lines)
}

// black list filter and then convert to bigbed (for true replicates only)
void filter_peak_and_convert_to_bigbed() { 
	if ( align ) return
	if ( !path_exists( blacklist ) ) return

	string[] filt_peaks
	for (int rep=1; rep<=get_num_rep(); rep++) {
		filt_peak_001 := \
			blacklist_filter_peak( "narrowPeak", peak_001{rep}, (peak_001{rep}).dirName(), "peak $rep" )

		wait
		// For ENCODE accession, use different step name for single rep case
		string ENCODE_step_name
		if ( is_input_fastq( rep ) ) {
			if ( get_num_rep() == 1 ) ENCODE_step_name = "anshul-kundaje:atac-seq-peaks-filter-step-run-single-rep-v1"
			else 			  ENCODE_step_name = "anshul-kundaje:atac-seq-peaks-filter-step-run-v1"
			add_ENCODE_metadata_to_summary_json( "bed", "narrowPeak", "raw peaks", \
				ENCODE_step_name, filt_peak_001, [filt_bam{rep}])



			if ( get_num_rep() == 1 ) ENCODE_step_name = "anshul-kundaje:atac-seq-signal-generation-step-run-single-rep-v1"
			else 			  ENCODE_step_name = "anshul-kundaje:atac-seq-signal-generation-step-run-v1"
			add_ENCODE_metadata_to_summary_json( "bigWig", "", "signal p-value", \
				ENCODE_step_name, pval_bigwig_001{rep}, [filt_bam{rep}])
			add_ENCODE_metadata_to_summary_json( "bigWig", "", "fold change over control", \
				ENCODE_step_name, fc_bigwig_001{rep}, [filt_bam{rep}])

			npeak_bb := peak_to_bigbed( "narrowPeak", filt_peak_001, filt_peak_001.dirName(), "peak $rep" )


			wait
			if ( get_num_rep() == 1 ) ENCODE_step_name = "anshul-kundaje:atac-seq-filtered-peaks-to-bigbed-step-run-single-rep-v1"
			else 			  ENCODE_step_name = "anshul-kundaje:atac-seq-filtered-peaks-to-bigbed-step-run-v1"
			add_ENCODE_metadata_to_summary_json( "bigBed", "narrowPeak", "raw peaks", \
				ENCODE_step_name, npeak_bb, [filt_peak_001])
		}
		filt_peaks.add(filt_peak_001)

	}

	wait

	// naive overlap peaks
	string ENCODE_peak_name
	if ( peak_overlap ) {
		if ( get_num_rep()==1 ) {
			ENCODE_step_name = "anshul-kundaje:atac-seq-overlap-step-run-single-rep-v1"
			ENCODE_peak_name = "pseudoreplicated stable peaks"
		}
		else {
			ENCODE_step_name = "anshul-kundaje:atac-seq-overlap-step-run-v1"
			ENCODE_peak_name = "replicated peaks"
		}
		add_ENCODE_metadata_to_summary_json( "bed", "narrowPeak", ENCODE_peak_name, \
			ENCODE_step_name, peak_overlap, filt_peaks )
		npeak_bb := peak_to_bigbed( "narrowPeak", peak_overlap, peak_overlap.dirName(), "peak overlap" )
		wait
		if ( get_num_rep()==1 )	ENCODE_step_name = "anshul-kundaje:atac-seq-stable-peaks-conversion-step-run-v1"
		else 			ENCODE_step_name = "anshul-kundaje:atac-seq-replicated-peaks-conversion-step-run-v1"
		add_ENCODE_metadata_to_summary_json( "bigBed", "narrowPeak", ENCODE_peak_name, \
			ENCODE_step_name, npeak_bb, [peak_overlap] )
	}

	wait

	print( "\n== Done filter_peak_and_convert_to_bigbed()\n" )	
}

void ataqc() {

	if ( no_ataqc || align ) return
	if ( is_input_peak() ) return

	for (int rep=1; rep<=get_num_rep(); rep++) {

		if ( no_par ) ataqc( rep )
		else 	  par ataqc( rep )
	}

	wait

	print( "\n== Done ataqc()\n" )
}

void ataqc( int rep ) {

	if ( true_rep ) {
		print("Warning: ATAQC cannot run with a flag -true_rep\n");
		return
	}
	if ( no_dup_removal ) {
		print("Warning: ATAQC cannot run with the flag -no_dup_removal\n");
		return
	}

	group := get_group_name( rep )
	long  := get_long_group_name( rep )

	qc_o_dir 	:= mkdir( "$out_dir/qc/$group" )
	aln_o_dir 	:= mkdir( "$out_dir/align/$group" ) // create align output directory

	if ( bam.hasKey(rep) ) {

		string idr_ataqc, peak

		if ( !enable_idr ) {
			idr_ataqc = ""
			peak = peak_overlap
		}
		else if ( get_num_rep() == 1 ) 	{
			idr_ataqc = idr_pr{1}
			peak = idr_pr{rep}
		}
		else {
			idr_ataqc = idr_opt
			peak = idr_pr{rep}
		}

		string ataqc_html

		if ( is_se( rep ) ) {

			( ataqc_html, ataqc_qc{rep} ) = ataqc( fastq{rep}, "", bam{rep}, align_log{rep}, pbc_qc{rep}, \
				dup_qc{rep}, filt_bam{rep}, final_tag{rep}, pval_bigwig_001{rep}, peak, \
				peak_overlap, idr_ataqc, qc_o_dir, group )
		}
		else {
			( ataqc_html, ataqc_qc{rep} ) = ataqc( fastq{rep+",1"}, fastq{rep+",2"}, bam{rep}, align_log{rep}, pbc_qc{rep}, \
				dup_qc{rep}, filt_bam{rep}, final_tag{rep}, pval_bigwig_001{rep}, peak, \
				peak_overlap, idr_ataqc, qc_o_dir, group )
		}

		add_file_to_report( ataqc_html, "ATAQC\\nreport", group, "QC and logs/ATAQC/$long/ATAQC HTML report" )
	}
}

void report() {

	wait

	string html
	html += html_title()
	html += html_cmd_line_args()
	html += html_conf_file_info()
	html += html_pipeline_version( "https://github.com/kundajelab/atac_dnase_pipelines/commit" ) // pipeline version info
	html += html_filetable() 	// treeview for directory and file structure
	html += html_atac_tracks() 	// epigenome browser tracks
	html += html_graph()	// graphviz workflow diagram
	html += html_atac_QC()	// show QC tables and images

	report( html )
	write_summary_json()

	print( "\n== Done report()\n" )
}

string html_atac_QC() {

	string[] align_qcs, flagstat_qcs, dup_qcs, flagstat_nodup_qcs, pbc_qcs, xcor_qcs, xcor_plots, ataqc_qcs
	string[] groups

	for ( int rep=1; rep <= get_num_rep(); rep++) {

		group := "rep$rep"
		key := "$rep"
		groups.add( group )

		if ( xcor_qc.hasKey( key ) )	{
			xcor_qcs 		+= xcor_qc{key}
			xcor_plots 		+= xcor_plot{key}
		}
		if ( flagstat_qc.hasKey( key ) )	flagstat_qcs 		+= flagstat_qc{key}
		if ( dup_qc.hasKey( key ) ) 		dup_qcs 		+= dup_qc{key}
		if ( flagstat_nodup_qc.hasKey( key ) )	flagstat_nodup_qcs 	+= flagstat_nodup_qc{key}
		if ( pbc_qc.hasKey( key ) ) 		pbc_qcs			+= pbc_qc{key}
		if ( ataqc_qc.hasKey( key ) )		ataqc_qcs		+= ataqc_qc{key}
	}

	html := "<div id='atac_qc'>"
	html += "<div style='float:left'>"
	html += html_table_multiple_logs( "Flagstat (raw) QC", false, "flagstat", groups, flagstat_qcs )
	html += "</div>"
	if ( !no_dup_removal ) {
		html += "<div style='float:left'>"
		html += html_table_multiple_logs( "Dup. QC", false, "dup", groups, dup_qcs )
		html += "</div>"
		html += "<div style='float:left'>"
		html += html_table_multiple_logs( "Flagstat (filtered) QC", false, "flagstat_filt", groups, flagstat_nodup_qcs )
		html += "</div>"
		html += "<div style='float:left'>"
		html += html_table_multiple_logs( "Library Complexity QC", false, has_pe() ? "pbc_PE" : "pbc", groups, pbc_qcs )
		if ( pbc_qcs.size()>0 ) html += html_help_pbc()
		html += "</div>"
	}
	html += "<div style='float:left'>"
	html += html_table_multiple_logs( "Enrichment QC (strand cross-correlation measures)", false, "xcor", groups, xcor_qcs )
	if ( xcor_qcs.size()>0 ) html += html_help_xcor( subsample_xcor, has_se(), has_pe() )
	html += "</div>"
	// xcor images
	for ( int i=0; i<xcor_plots.size(); i++ ) {
		png := pdf_to_png( xcor_plots[i] )
		html += html_img( png, 500, groups[i] ) + "&nbsp"
	}
	// number of peaks
	if ( num_peak_log ) {
		html += "<div style='float:left'>"
		html += html_table_multiple_logs( "Number of peaks", false, "num_peaks", num_peak_log )
		html += html_help_num_peaks()
		html += "</div>"
	}	
	// IDR FRiP
	if ( idr_qc_FRiP.size()>0 ) {
		html += "<div style='float:left'>"
		html += html_table_multiple_logs( "Enrichment QC (Fraction of reads in peaks)", false, "idr_FRiP", idr_qc_FRiP )
		html += html_help_idr_FRiP()
		html += "</div>"
	}
	// if idr qc's exists, add them to html
	if ( idr_qc != "" ) {
		html += "<div style='float:left'>"
		html += html_table_multiple_logs( "Reproducibility QC and Peak Detection Statistics (Irreproducible Discovery Rate)", \
							false, "idr", ["rep1"], [idr_qc] )
		html += html_help_idr( idr_thresh )
		html += "</div>"
	}
	for ( int i=1; i<=get_num_rep(); i++ ) {
		for ( int j=i+1; j<=get_num_rep(); j++ ) {
			if ( idr_tr_png.hasKey("$i,$j") ) \
				html += html_img( idr_tr_png{"$i,$j"}, 800, "true reps (rep$i-rep$j)" ) + "&nbsp"
		}
	}
	if ( idr_ppr_png != "" ) html += html_img( idr_ppr_png, 800, "pooled pseudo-reps" ) + "&nbsp"
	for ( int i=1; i<=get_num_rep(); i++ ) {
		      if ( idr_pr_png.hasKey(i) ) \
				html += html_img( idr_pr_png{i}, 800, "rep$i pseudo-reps" ) + "&nbsp"
	}
	html += "<div style='float:left'>"
	html += html_table_multiple_logs( "ATAQC", false, "ataqc", groups, ataqc_qcs )
	html += "</div>"

	html += "</div><br>"
	return html
}

string html_atac_tracks() {
	if ( no_browser_tracks ) return ""

	string[] trk_files, trk_types, trk_names, trk_colors
	string color

	// for (int rep=1; rep<=get_num_rep(); rep++) {
	// 	color = get_predefined_rgb_str( rep )
	// 	if ( bam.hasKey(rep) ) { trk_types += "bam"; trk_names += "$title bam (rep$rep)"; trk_colors += color; trk_files += bam{rep} }
	// }

	color = get_predefined_rgb_str( 0 ) // color for pooled reps
	if ( pval_bigwig_001.hasKey( "pooled" ) ) { trk_types += "bigwig"; trk_names += "$title pval (pooled)"; trk_colors += color; trk_files += pval_bigwig_001{"pooled"} }
	if ( peak_overlap != "" ) { trk_types += "hammock"; trk_names += "$title peak overlap"; trk_colors += color; trk_files += peak_to_hammock( peak_overlap ) }


	for (int rep=1; rep<=get_num_rep(); rep++) {
		color = get_predefined_rgb_str( rep )
		if ( pval_bigwig_001.hasKey( "$rep" ) ) { trk_types += "bigwig"; trk_names += "$title pval (rep$rep)"; trk_colors += color; trk_files += pval_bigwig_001{rep} }
		if ( peak_001.hasKey( "$rep" ) ) { trk_types += "hammock"; trk_names += "$title peak (rep$rep)"; trk_colors += color; trk_files += peak_to_hammock( peak_001{rep} ) }
		if ( idr_pr.hasKey(rep) ) {	trk_types += "hammock"; trk_names += "$title peak idr (rep$rep-pr)"; trk_colors += color; trk_files += peak_to_hammock( _get_idr_peak_trk( idr_pr{rep} ) ) }
	}

	html := html_epg_browser_viz( trk_files, trk_types, trk_names, trk_colors, species_browser )

	return html
}

void help() {

	if ( is_cmd_line_arg_empty() ) {

		printHelp()
		exit
	}
}

bool is_atac_seq() {

	return type.toLower().startsWith( "atac" )
}



void tar_all_logs() {
	if ( enable_idr && idr_qc && idr_opt && idr_consv ) {
		// *.align.log: bowtie2 log
		// *_qc.txt: ATAQC text report
		// *_qc.html: ATAQC HTML report
		string tar
		mkdir("$out_dir/qc")
		if ( title ) tar = "$out_dir/qc/$title.all_quality_metrics.tar"
		else tar = "$out_dir/qc/all_quality_metrics.tar"

		taskName:= "tar_all_logs"
		system 	:= "local"
		tid := task {
			sys cd $out_dir
			sys find . -type f \
				-name '*.align.log' -or \
				-name '*.dot' -or \
				-name '*.svg' -or \
				-name '*.css' -or \
				-name '*.json' -or \
				-name '*.html' -or \
				-name '*.js' -or \
				-name '*.qc' -or \
				-name '*.pdf' -or \
				-name '*.png' -or \
				-name '*_qc.txt' -or \
				-name '*read_length.txt' \
				-name '*number_of_peaks.txt' \
				| xargs tar -cvf $tar
		}

		wait

		string[] quality_metric_of
		string ENCODE_step_name
		if ( idr_pr.hasKey(1) && get_num_rep()==1 ) {
			ENCODE_step_name = "anshul-kundaje:atac-seq-unreplicated-idr-step-run-single-rep-v1"
			quality_metric_of.add(idr_pr{1})
		}
		else {
			ENCODE_step_name = "anshul-kundaje:atac-seq-idr-step-run-v1"
			quality_metric_of.add(idr_opt)
		}
		add_ENCODE_quality_metrics_to_summary_json( "generic_quality_metric", ENCODE_step_name, \
								quality_metric_of, [tar] )
	}
}


